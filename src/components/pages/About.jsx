import React, { Component, Fragment } from "react";
import { Redirect } from "react-router-dom";
import { Container } from "reactstrap";
import Navigation from "../Navigation";

class About extends Component {
  state = {
    image: null,
    isAuthenticated: false,
  };
  componentWillMount() {
    this.checkUser();
  }

  checkUser = () => {
    const token = localStorage.getItem("token");
    if (token) {
      return this.setState({ isAuthenticated: true });
    }
  };

  handleLogout = () => {
    localStorage.removeItem("token");
    const { history } = this.props;
    history.push("/login");
  };

  render() {
    const { image, isAuthenticated } = this.state;
    if (!isAuthenticated) {
      return <Redirect to="/login" />;
    }

    return (
      <Fragment>
        <Navigation />
        <Container className="p-4">
          <h1>This is About</h1>
          <button className="btn btn-danger" onClick={this.handleLogout}>
            Logout
          </button>
        </Container>
      </Fragment>
    );
  }
}

export default About;
