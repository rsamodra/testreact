import React, { Component, Fragment } from "react";
import { Container, Card, CardBody, CardTitle } from "reactstrap";
import Navigation from "../Navigation";

class Post extends Component {
  render() {
    const { title } = this.props;
    return <h5>{title}</h5>;
  }
}

class AnotherHome extends Component {
  state = {
    posts: [],
  };
  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then((response) => response.json())
      .then((result) => {
        this.setState({ posts: result });
      });
  }
  render() {
    // const { posts } = this.state;
    return (
      <Fragment>
        <Navigation />
        <Container className="p-4">
          <h2>this is Another Home</h2>
          {this.state.posts.map((post) => {
            return (
              <Card>
                <CardBody>
                  <CardTitle>{post.title}</CardTitle>
                </CardBody>
              </Card>
            );
          })}
        </Container>
      </Fragment>
    );
  }
}

export default AnotherHome;
