import React, { Component, Fragment } from "react";
import { Container } from "reactstrap";
import NavbarKu from "../Navigation";

class NotFound extends Component {
  render() {
    return (
      <Fragment>
        <NavbarKu />
        <Container className="p-4">
          <h1>PAGE NOT FOUND</h1>
        </Container>
      </Fragment>
    );
  }
}

export default NotFound;
