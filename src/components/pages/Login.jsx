import { Component } from "react";
import { Container } from "reactstrap";

class Response extends String {
  json = () => JSON.parse(this);
}

function mockFetch(url, { body }) {
  const { email, password } = body;
  console.log(email, password);

  if (email !== "aku@binar.com" || password !== "aku") {
    return Promise.reject("Invalid email or password");
  }
  return Promise.resolve(
    new Response(JSON.stringify({ accessToken: "tokenku" }))
  );
}

class Login extends Component {
  state = {
    email: null,
    password: null,
  };

  setAku = (name) => (event) => this.setState({ [name]: event.target.value });

  handleSubmit = (event) => {
    event.preventDefault();
    const { email, password } = this.state;
    const { history } = this.props;
    mockFetch("https://api.tbd.com/auth", { body: { email, password } })
      .then((response) => response.json())
      .then((responseBody) => {
        localStorage.setItem("token", responseBody.accessToken);
        history.push("/");
      })
      .catch((err) => {
        alert(err);
      });
  };

  render() {
    return (
      <Container className="p-4 d-flex align-items-center justify-content-center">
        <h1>Login</h1>
        <form onSubmit={this.handleSubmit}>
          <label>
            Email :
            <input type="email" name="email" onChange={this.setAku("email")} />
          </label>
          <label>
            Password :
            <input
              type="password"
              name="password"
              onChange={this.setAku("password")}
            />
          </label>
          <input type="submit" value="submit" />
        </form>
      </Container>
    );
  }
}

export default Login;
