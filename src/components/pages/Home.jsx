import React, { Component, Fragment } from "react";
import { Container } from "reactstrap";
import Navigation from "../Navigation";

class Home extends Component {
  render() {
    return (
      <Fragment>
        <Navigation />
        <Container className="p-4">
          <h1>This is Home</h1>
        </Container>
      </Fragment>
    );
  }
}

export default Home;
