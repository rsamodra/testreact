import React, { Component } from "react";
import {
  Collapse,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
} from "reactstrap";
import { Link } from "react-router-dom";

class Navigation extends Component {
  state = { isOpen: false };
  toggleCollapse = () => {};

  render() {
    const { isOpen } = this.state;
    return (
      <Navbar color="dark" dark expand="lg">
        <NavbarBrand href="/">Binar Academy</NavbarBrand>
        <NavbarToggler onClick={this.toggleCollapse} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem className="px-2 banner-link">
              <Link to="/">Home</Link>
            </NavItem>
            <NavItem className="px-2">
              <Link to="/about">About</Link>
            </NavItem>
            <NavItem className="px-2">
              <Link to="/contact">Contact</Link>
            </NavItem>
            <NavItem className="px-2">
              <Link to="/AnotherHome">AnotherHome</Link>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    );
  }
}

export default Navigation;
