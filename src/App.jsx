import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Home from "./components/pages/Home";
import About from "./components/pages/About";
import Contact from "./components/pages/Contact";
import AnotherHome from "./components/pages/AnotherHome";
import NotFound from "./components/pages/NotFound";
import Login from "./components/pages/Login";

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/anotherhome" component={AnotherHome} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    );
  }
}

export default App;
