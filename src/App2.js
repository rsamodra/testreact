import React, { Component } from "react";
import { Alert, Button, Container } from "reactstrap";

class App extends Component {
  state = { danger: false };

  handleOnClick = () => {
    this.setState({ danger: true });
  };
  render() {
    const { danger } = this.state;

    return (
      <Container>
        <Container className="text-center">
          <Button className="mb-4" color="danger" onClick={this.handleOnClick}>
            Pencet Aku
          </Button>
          {danger && <Alert color="danger">ishishish</Alert>}
        </Container>
      </Container>
    );
  }
}

export default App;
